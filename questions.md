- Many towns and cities in North America have a _Main Street_. Why is it seemingly always a Street, and we never see _Main Avenue_ or _Main Road_?

- Is there a logic to the system seen in government and other large institutions where a budget must be fully spent each year, under threat of having it reduced if it is not fully spent? Further, why are mechanisms for carrying the surplus forward to future years specifically banned?
 
 - Why do babies scream uncontrollably, despite the best efforts of the parent to quiet them? This seems it certainly would have created dangerous situations for early humans, _e.g._ hiding from predators or other hostile humans.

- What is the fate of skyscrapers? Are they effectively immortal? If not, is there a well-defined plan to safely demolish them when they reach end-of-life decades or centuries from their constructions, or is there no plan, and it's left as a problem for future generations?

- How do airliners validate credit card transactions in flight?

- What is the mortality rate of being in Disneyworld or Disneyland? The combination of (presumably) greater-than-average age and obesity rates, high temperatures, and huge numbers of people must mean that people regularly die in the Happiest Place on Earth. How many?

- What is the most recently invented family name derived from a vocation or location?

- Companies pay dividends or buy back shares when it has profits to distribute
  but no way to grow its business, however, they often do this while still
  having outstanding debt. Wouldn't buying back bonds first be an efficient way
  of growing the business?

- What happens in surgical theatres during some sudden, unexpected event? If
  there is a fire alarm, do the surgeons abandon the patient? In seismically
  active areas, are they isolated from tremors?

- Why do cats vomit so much? This seems disadvantageous to a stealthy predator. Is it a product of their domestic lifestyle? Do wildcats cough up hairballs too?

- Why aren't trains autonomous?

- Why are so many harmful activities enjoyable / why are enjoyable activities
  harmful? e.g. Alcohol, loud music, thrill-seeking, etc.

- Does the tempo or rhythm or music affect non-human mammals in any way? If you
  played at equal volumes Erik Satie to one barn and Rage Against the Machine to
  another barn, would the animals in the show any difference in, for example,
  the ability to sleep?

- How can a surface be simultaneously both shiny and black?
