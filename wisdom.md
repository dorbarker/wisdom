- `pdfcrop` is a command-line tool included with `texlive` that will crop a PDF to its smallest possible bounding box.

- `find foo/ -name "*.bar" -exec some_program "{}" +` can be used to get around maximum argument limits _versus_ `some_program foo/*.bar` on large directories.

- Japanese fountain pens produce narrower lines for the same nominal nib size
  than their European counterparts.

- In the Prisoner's Dilemma, a tit-for-tat strategy is best in environments
  where there is a large degree of trust. However, it is good to allow
  forgiveness to prevent accidental escalation. 

- The global .gitignore is located at `$HOME/.config/git/ignore`

- _Campylobacter jejuni_ is NCBI taxid 197

- A Risset Rhythm is analgous to a Shepard Tone, but gives the impression of infinite acceleration of the tempo

- A micromort is a unit of acute risk equivalent to a 1-in-1000000 chance of death. A microlife is a unit of chronic risk equivalent to a change of 0.5 hours of life expectancy.

- To read each line of a delimited file into an array in `bash`:  
    ```
    # change IFS to your delimiter character  
    while IFS=$'\t' read -r -a line; do  
        # $line holds the array  
        echo ${line[0]}  
    done < some_file.tsv  
    ```

- `cat -vet` will explicitly show tab-characters (^I) and line endings ($) in
  output, and is useful for debugging file formats

- Tiresias is a GPL-licenced font designed to maximally readable

- Append `bind 'set bell-style none'` to your `~/.bashrc` to disable the terminal thunk
